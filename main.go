package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Testpage for Commenting System")
	fmt.Println("Endpoint Hit: homePage")
}

var sqliteDatabase *sql.DB

func handleRequests() {

	myRouter := mux.NewRouter().StrictSlash(true)

	// Endpoints
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/comments", returnAllComments)
	myRouter.HandleFunc("/postcomment", createNewComment).Methods("POST")
	myRouter.HandleFunc("/comment/{id}", returnSingleComment).Methods("GET")
	myRouter.HandleFunc("/commentthread/{id}", returnThreadComments).Methods("GET")
	myRouter.HandleFunc("/deletecomment/{id}/{secret}", deleteComment)
	log.Fatal(http.ListenAndServe(":4000", myRouter))
}

// Comment basic structure
type Comment struct {
	ID     string `json:"ID"`
	Parent string `json:"Parent"`
	Msg    string `json:"Msg"`
	Author string `json:"Author"`
	Time   string `json:"Time"`
}
type incomingComment struct {
	Parent string `json:"Parent"`
	Msg    string `json:"Msg"`
	Author string `json:"Author"`
}

func returnAllComments(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnAllArticles")
	json.NewEncoder(w).Encode(displayAll(sqliteDatabase))
}

func returnSingleComment(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint for Single Comment")
	vars := mux.Vars(r)
	key := vars["id"]

	json.NewEncoder(w).Encode(displaySingle(sqliteDatabase, key))
}

func returnThreadComments(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint for Thread Comments")
	vars := mux.Vars(r)
	key := vars["id"]
	json.NewEncoder(w).Encode(displayParent(sqliteDatabase, key))
}

func createNewComment(w http.ResponseWriter, r *http.Request) {
	fmt.Println("trying to create a comment")
	reqBody, _ := ioutil.ReadAll(r.Body)
	var comment incomingComment
	json.Unmarshal(reqBody, &comment)

	insert(sqliteDatabase, comment.Parent, comment.Msg, comment.Author)

	json.NewEncoder(w).Encode(comment)
}

func deleteComment(w http.ResponseWriter, r *http.Request) {
	// once again, we will need to parse the path parameters
	vars := mux.Vars(r)
	// we will need to extract the `id` of the article we
	// wish to delete
	id := vars["id"]
	secret := vars["secret"]

	if secret != "yoursecrethere" {
		return
	}
	delete(sqliteDatabase, id)

}

func main() {
	fmt.Println("Starting Router.")
	sqliteDatabase = initDB()

	handleRequests()
}
