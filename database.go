package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
)

func initDB() *sql.DB {

	if _, err := os.Stat("./comments-db.db"); os.IsNotExist(err) {
		log.Println("Creating comments-db.db...")
		file, err := os.Create("comments-db.db") // Create SQLite file
		if err != nil {
			log.Fatal(err.Error())
		}
		file.Close()
		log.Println("comments-db.db created")

		sqliteDatabase, _ := sql.Open("sqlite3", "./comments-db.db") // Open the created SQLite File
		defer sqliteDatabase.Close()                                 // Defer Closing the database
		createTable(sqliteDatabase)
	} else {
		fmt.Println("database found")
	}
	sqliteDatabase, _ := sql.Open("sqlite3", "./comments-db.db")
	return sqliteDatabase
}

func createTable(db *sql.DB) {
	createTableSQL := `CREATE TABLE Comment (
		"ID" integer NOT NULL PRIMARY KEY AUTOINCREMENT,		
		"Parent" TEXT,
		"Msg" TEXT,
		"Author" TEXT,
		"Time" TEXT		
	  );`

	log.Println("Create comments table...")
	statement, err := db.Prepare(createTableSQL)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec()
	log.Println("table created")
}

func insert(db *sql.DB, Parent string, Msg string, Author string) {
	log.Println("Inserting comment record ...")
	insertSQL := `INSERT INTO Comment(Parent, Msg,Author, Time) VALUES (?, ?,?, CURRENT_TIME)`
	statement, err := db.Prepare(insertSQL) //good to avoid SQL injections
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = statement.Exec(Parent, Msg, Author)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func delete(db *sql.DB, key string) {
	log.Println("Deleting comment record ...")
	insertSQL := `DELETE FROM Comment WHERE ID=?`
	statement, err := db.Prepare(insertSQL) //good to avoid SQL injections
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = statement.Exec(key)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func displayAll(db *sql.DB) []Comment {
	row, err := db.Query("SELECT * FROM Comment ORDER BY Msg")
	if err != nil {
		log.Fatal(err)
	}
	defer row.Close()

	var tmpA []Comment
	for row.Next() {
		var tmp Comment
		row.Scan(&tmp.ID, &tmp.Parent, &tmp.Msg, &tmp.Author, &tmp.Time)
		tmpA = append(tmpA, tmp)
	}
	return tmpA
}

func displaySingle(db *sql.DB, key string) Comment {
	fmt.Println(key)
	QuerySQL := `SELECT * FROM Comment WHERE ID=? ORDER BY Msg`
	statement, err := db.Prepare(QuerySQL) // Prepare statement. This is good to avoid SQL injections
	if err != nil {
		log.Fatalln(err.Error())
	}
	var tmp Comment
	err = statement.QueryRow(key).Scan(&tmp.ID, &tmp.Parent, &tmp.Msg, &tmp.Author, &tmp.Time)

	if tmp == (Comment{}) {
		fmt.Print("No Comment found or empty")
	}
	return tmp
}

func displayParent(db *sql.DB, parent string) []Comment {

	QuerySQL := `SELECT * FROM Comment WHERE Parent=? ORDER BY Msg`
	statement, err := db.Prepare(QuerySQL)
	if err != nil {
		log.Fatalln(err.Error())
	}

	row, err := statement.Query(parent)
	if err != nil {
		log.Fatal(err)
	}
	defer row.Close()
	var tmpA []Comment
	for row.Next() {
		var tmp Comment
		row.Scan(&tmp.ID, &tmp.Parent, &tmp.Msg, &tmp.Author, &tmp.Time)
		tmpA = append(tmpA, tmp)
	}
	return tmpA
}
